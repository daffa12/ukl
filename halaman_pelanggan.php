<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "rent_car";
$conn = new mysqli($servername, $username, $password, $dbname);

$sql = "SELECT * FROM mobil";
$result = $conn->query($sql);?>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="asset/halaman_pelanggan.css">
    <link rel="stylesheet" href="asset/bootstrap.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-primary">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Pelanggan
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="logout.php">Log Out</a>
        </div>
      </li>
    </ul>
  </div>
</nav>
   <Section id="box"> 

    <h2>List Mobil</h2>
<form action="cari_id_mobil.php" method="get">
<input type="text" name="cari">
<select name ="tampil">
    <option value ="id">Tampilkan id</option>
    <input type="submit" value="Cari">
</form>
 
    <form>
        <table class="table1"> 
            <tr >
                <th>id_mobil</th>
                <th>No Mobil </th>
                <th>Merk</th>
                <th>Jenis</th>
                <th>Warna</th>
                <th>Tahun Pembuatan</th>
                <th>Biaya Sewa Per Hari</th>
                <th></th>
            </tr>
            
                <?php while ($row = $result->fetch_assoc()) {?>
                <tr>
                    <td><?php echo $row ["id_mobil"] ?></td>
                    <td><?php echo $row ["nomor_mobil"] ?></td>
                    <td><?php echo $row ["merk"] ?></td>
                    <td><?php echo $row ["jenis"] ?></td>
                    <td><?php echo $row ["warna"] ?></td>
                    <td><?php echo $row ["tahun_pembuatan"] ?></td>
                    <td><?php echo $row ["biaya_sewa_per_hari"] ?></td>
                    <td><?php echo "<a href ='mobil_sewa.php?id=$row[id_mobil]'>Sewa</a><br>"; ?></td>
                </tr>
                <?php }?>
        </table>
    </form>
    </Section>
    <script src="js/bootstrap.js"></script>
        <script src="js/jquery.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>